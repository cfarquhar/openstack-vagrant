#!/bin/bash +x

PERSISTENT_DATA_DIR=~/openstack-vagrant/persistent-data

# Make sure we're not going to overwrite anything we shouldn't
read -p "We may overwrite data in \"$PERSISTENT_DATA_DIR\", is this okay? " -n 1 -r
echo
if [[ $REPLY =~ [^Yy]$ ]]
then
    exit 1
fi

# Create a directory to share apt cache between VMs
mkdir -p $PERSISTENT_DATA_DIR
mkdir -p $PERSISTENT_DATA_DIR/var/cache/apt/archives/partial

# Download chef_server.deb
wget --no-check-certificate -O $PERSISTENT_DATA_DIR/chef_server.deb http://opscode-omnibus-packages.s3.amazonaws.com/ubuntu/12.04/x86_64/chef-server_11.0.8-1.ubuntu.12.04_amd64.deb

# Download images
wget --no-check-certificate -O $PERSISTENT_DATA_DIR/cirros-0.3.0-x86_64-disk.img http://launchpad.net/cirros/trunk/0.3.0/+download/cirros-0.3.0-x86_64-disk.img
wget --no-check-certificate -O $PERSISTENT_DATA_DIR/precise-server-cloudimg-amd64-disk1.img http://uec-images.ubuntu.com/precise/current/precise-server-cloudimg-amd64-disk1.img

# Clone/download cookbooks
mkdir -p $PERSISTENT_DATA_DIR/opt/rpcs
git clone -b grizzly git://github.com/rcbops/chef-cookbooks.git $PERSISTENT_DATA_DIR/opt/rpcs/chef-cookbooks
cd $PERSISTENT_DATA_DIR/opt/rpcs/chef-cookbooks
git checkout 5bc4ce611eb64fb716583fda1a12a3700e41a3bf
git submodule init
git submodule update
git clone --depth 1 https://github.com/opscode-cookbooks/cron.git $PERSISTENT_DATA_DIR/opt/rpcs/chef-cookbooks/cookbooks/cron
git clone --depth 1 https://github.com/opscode-cookbooks/chef-client.git $PERSISTENT_DATA_DIR/opt/rpcs/chef-cookbooks/cookbooks/chef-client
mkdir -p $PERSISTENT_DATA_DIR/opt/rpcs/chef-cookbooks/cookbooks/rpc-support
curl http://cookbooks.howopenstack.org/rpc-support.tar.gz | tar zxf - -C $PERSISTENT_DATA_DIR/opt/rpcs/chef-cookbooks/cookbooks/rpc-support
wget http://cookbooks.howopenstack.org/roles/rpc-support.rb -O $PERSISTENT_DATA_DIR/opt/rpcs/chef-cookbooks/roles/rpc-support.rb

