#!/bin/bash -x

sudo su -
ifup eth1

apt-get update 
#apt-get -y upgrade
apt-get install -y rabbitmq-server git curl 

export CHEF_RMQ_PW=`date | md5sum | cut -d' ' -f1`
rabbitmqctl add_vhost /chef
rabbitmqctl add_user chef $CHEF_RMQ_PW
rabbitmqctl set_permissions -p /chef chef '.*' '.*' '.*'
#wget -O /tmp/chef_server.deb https://opscode-omnibus-packages.s3.amazonaws.com/ubuntu/12.04/x86_64/chef-server_11.0.8-1.ubuntu.12.04_amd64.deb
cp /vagrant/persistent-data/chef_server.deb /tmp/
dpkg -i /tmp/chef_server.deb
mkdir /etc/chef-server
cat > /etc/chef-server/chef-server.rb <<EOF
nginx["ssl_port"] = 4000
nginx["non_ssl_port"] = 4080
nginx["enable_non_ssl"] = true
rabbitmq["enable"] = false
rabbitmq["password"] = "$CHEF_RMQ_PW"
#bookshelf['url'] = "https://#{node['ipaddress']}:4000"
bookshelf['url'] = "https://10.240.0.2:4000"
EOF
chef-server-ctl reconfigure

bash <(wget -O - http://opscode.com/chef/install.sh)
mkdir /root/.chef
cat > /root/.chef/knife.rb <<EOF
log_level                :info
log_location             STDOUT
node_name                'admin'
client_key               '/etc/chef-server/admin.pem'
validation_client_name   'chef-validator'
validation_key           '/etc/chef-server/chef-validator.pem'
chef_server_url          'https://localhost:4000'
cache_options( :path => '/root/.chef/checksums' )
cookbook_path            [ '/opt/rpcs/chef-cookbooks/cookbooks' ]
EOF

mkdir -p /opt/rpcs
#git clone --recursive --depth 1 -b grizzly http://github.com/rcbops/chef-cookbooks /opt/rpcs/chef-cookbooks
#git clone --depth 1 https://github.com/opscode-cookbooks/cron.git /opt/rpcs/chef-cookbooks/cookbooks/cron
#git clone --depth 1 https://github.com/opscode-cookbooks/chef-client.git /opt/rpcs/chef-cookbooks/cookbooks/chef-client
#mkdir -p /opt/rpcs/chef-cookbooks/cookbooks/rpc-support
#curl http://cookbooks.howopenstack.org/rpc-support.tar.gz | tar zxf - -C /opt/rpcs/chef-cookbooks/cookbooks/rpc-support
#wget http://cookbooks.howopenstack.org/roles/rpc-support.rb -O /opt/rpcs/chef-cookbooks/roles/rpc-support.rb
cp -a /vagrant/persistent-data/opt/rpcs/* /opt/rpcs
knife cookbook upload -a
knife role from file /opt/rpcs/chef-cookbooks/roles/*.rb

#curl --silent https://raw.github.com/DavidWittman/openstack-chef-deploy/master/environments/grizzly-neutron.json | sed "s/__CHANGE_ME__/$(tr -dc 'a-zA-Z0-9' < /dev/urandom | head -c 24)/" > rpcs.json
cp /vagrant/persistent-data/grizzly-neutron.json /root/rpcs.json
knife environment from file /root/rpcs.json

knife bootstrap localhost -E rpcs -r 'role[single-controller],role[single-network-node],role[rpc-support]'

cat > /etc/network/interfaces << EOF
auto eth0
iface eth0 inet dhcp

auto eth1
iface eth1 inet manual
    up ip link set \$IFACE up
    down ip link set \$IFACE down

iface br-eth1 inet static
    address 10.240.0.2
    netmask 255.255.255.0
    gateway 10.240.0.1
    dns-nameservers 8.8.8.8
EOF

cat > /etc/rc.local << EOF
ifup eth0
ifup br-eth1
EOF

update-rc.d -f openvswitch-switch remove
update-rc.d openvswitch-switch stop 20 0 1 6 . start 19 2 3 4 5 .

ovs-vsctl add-port br-eth1 eth1

ifdown --force eth1
ifup eth1
ifup br-eth1

/etc/init.d/networking restart

. ~/openrc

PRIVATE_NET_UUID=$(quantum net-create --provider:physical_network=ph-eth1 --provider:network_type=vlan --provider:segmentation_id=6 --shared private | awk '/ id /{print $4}')
quantum subnet-create private 10.241.0.0/24 --name private --no-gateway --host-route destination=0.0.0.0/0,nexthop=10.241.0.1 --allocation-pool start=10.241.0.11,end=10.241.0.254 --dns-nameserver 8.8.8.8

PRIVATE2_NET_UUID=$(quantum net-create --provider:physical_network=ph-eth1 --provider:network_type=vlan --provider:segmentation_id=7 --shared private2 | awk '/ id /{print $4}')
quantum subnet-create private2 10.242.0.0/24 --name private2 --no-gateway --host-route destination=0.0.0.0/0,nexthop=10.242.0.1 --allocation-pool start=10.242.0.11,end=10.242.0.254 --dns-nameserver 8.8.8.8

PRIVATE3_NET_UUID=$(quantum net-create --provider:physical_network=ph-eth1 --provider:network_type=vlan --provider:segmentation_id=8 --shared private3 | awk '/ id /{print $4}')
quantum subnet-create private3 10.243.0.0/24 --name private3 --no-gateway --host-route destination=0.0.0.0/0,nexthop=10.243.0.1 --allocation-pool start=10.243.0.11,end=10.243.0.254 --dns-nameserver 8.8.8.8

nova flavor-create --is-public True tiny-test 6 256 10 1

nova keypair-add --pub-key ~/.ssh/id_rsa.pub controller-id_rsa

CIRROS_UUID=$(glance image-create --disk-format qcow2 --container-format bare --name "cirros-0.3.0-x86_64" --file /vagrant/persistent-data/cirros-0.3.0-x86_64-disk.img --is-public true)
PRECISE_UUID=$(glance image-create --disk-format qcow2 --container-format bare --name "Ubuntu 12.04.1 Precise (cloudimg)" --file /vagrant/persistent-data/precise-server-cloudimg-amd64-disk1.img --is-public true)

nova secgroup-add-rule default tcp 22 22 0.0.0.0/0
nova secgroup-add-rule default icmp -1 -1 0.0.0.0/0
