#!/bin/bash -x

echo "Performing postkick steps for compute02"

ifup eth1

ssh -oStrictHostKeyCHecking=no root@controller01 knife bootstrap compute02 -E rpcs -r 'role[single-compute],role[rpc-support]' --server-url https://10.240.0.2:4000

cat > /etc/network/interfaces << EOF
auto eth0
iface eth0 inet dhcp

auto eth1
iface eth1 inet manual
    up ip link set \$IFACE up
    down ip link set \$IFACE down

iface br-eth1 inet static
    address 10.240.0.4
    netmask 255.255.255.0
    gateway 10.240.0.1
    dns-nameservers 8.8.8.8
EOF

update-rc.d -f openvswitch-switch remove
update-rc.d openvswitch-switch stop 20 0 1 6 . start 19 2 3 4 5 .

ovs-vsctl add-port br-eth1 eth1

ifdown --force eth1
ifup br-eth1

/etc/init.d/networking restart
