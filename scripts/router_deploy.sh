#!/bin/bash

SHELL_API=/bin/cli-shell-api

# Obtain session environment
session_env=$($SHELL_API getSessionEnv $PPID)

# Evaluate environment string
eval $session_env

# Setup the session
cli-shell-api setupSession

cli-shell-api inSession
if [ $? -ne 0 ]; then
    echo "Something went wrong!"
fi

SET=${vyatta_sbindir}/my_set
DELETE=${vyatta_sbindir}/my_delete
COPY=${vyatta_sbindir}/my_copy
MOVE=${vyatta_sbindir}/my_move
RENAME=${vyatta_sbindir}/my_rename
ACTIVATE=${vyatta_sbindir}/my_activate
DEACTIVATE=${vyatta_sbindir}/my_activate
COMMENT=${vyatta_sbindir}/my_comment
COMMIT=${vyatta_sbindir}/my_commit
DISCARD=${vyatta_sbindir}/my_discard
SAVE=${vyatta_sbindir}/vyatta-save-config.pl

#$SET interfaces ethernet eth0 dhcp
#$COMMENT interfaces ethernet eth0 "Default gateway"

$SET interfaces ethernet eth1 address 10.240.0.1/24
$COMMENT interfaces ethernet eth1 "Host network"

$SET interfaces ethernet eth1 vif 6 address 10.241.0.1/24
$COMMENT interfaces ethernet eth1 vif 6 "Provider network 1"

$SET interfaces ethernet eth1 vif 7 address 10.242.0.1/24
$COMMENT interfaces ethernet eth1 vif 7 "Provider network 2"

$SET interfaces ethernet eth1 vif 8 address 10.243.0.1/24
$COMMENT interfaces ethernet eth1 vif 8 "Provider network 3"

### Set up DHCP server on host network
$SET service dhcp-server shared-network-name HOST subnet 10.240.0.0/24 start 10.240.0.50 stop 10.240.0.100
$SET service dhcp-server shared-network-name HOST subnet 10.240.0.0/24 dns-server 8.8.8.8
#$SET service dhcp-server shared-network-name HOST subnet 10.240.0.0/24 default-router 10.240.0.1

### Set up static DHCP mappings
## This shouldn't be needed once everything moves to statics
$SET service dhcp-server shared-network-name HOST subnet 10.240.0.0/24 static-mapping controller01 ip-address 10.240.0.2
$SET service dhcp-server shared-network-name HOST subnet 10.240.0.0/24 static-mapping controller01 mac-address 02:00:00:00:01:02

$SET service dhcp-server shared-network-name HOST subnet 10.240.0.0/24 static-mapping compute01 ip-address 10.240.0.3
$SET service dhcp-server shared-network-name HOST subnet 10.240.0.0/24 static-mapping compute01 mac-address 02:00:00:00:02:02

$SET service dhcp-server shared-network-name HOST subnet 10.240.0.0/24 static-mapping compute02 ip-address 10.240.0.4
$SET service dhcp-server shared-network-name HOST subnet 10.240.0.0/24 static-mapping compute02 mac-address 02:00:00:00:03:02

### Set up masquerading for instances
$SET nat source rule 10 source address 10.241.0.0/24
$SET nat source rule 10 translation address masquerade
$SET nat source rule 10 outbound-interface eth0
$SET nat source rule 20 source address 10.241.0.0/24
$SET nat source rule 20 translation address masquerade
$SET nat source rule 20 outbound-interface eth0

$SET nat source rule 30 source address 10.242.0.0/24
$SET nat source rule 30 translation address masquerade
$SET nat source rule 30 outbound-interface eth0
$SET nat source rule 40 source address 10.242.0.0/24
$SET nat source rule 40 translation address masquerade
$SET nat source rule 40 outbound-interface eth0

$SET nat source rule 50 source address 10.243.0.0/24
$SET nat source rule 50 translation address masquerade
$SET nat source rule 50 outbound-interface eth0
$SET nat source rule 60 source address 10.243.0.0/24
$SET nat source rule 60 translation address masquerade
$SET nat source rule 60 outbound-interface eth0

### Set up masquerading for hosts
$SET nat source rule 70 source address 10.240.0.0/24
$SET nat source rule 70 translation address masquerade
$SET nat source rule 70 outbound-interface eth0


$COMMIT
$SAVE
