#!/bin/bash

mv ./precise64_os.box{,$$}
vagrant package --base precise64_os --output ./precise64_os.box
vagrant box add precise64_os ./precise64_os.box -f
