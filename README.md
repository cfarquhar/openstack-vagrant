# Overview
## What is it?

This uses a vyatta router and Ubuntu baseboxes to deploy OpenStack dev/test environments in VirtualBox using the RPC cookbooks and deployment methods.

## Benefits

 * Quickly test changes to deployment process or rcbops/rpc-support cookbooks
 * Provides a learning environment that is easy to reset after breaking everything

# Installation

## Install dependencies
 * Virtualbox - https://www.virtualbox.org/wiki/Downloads
 * Vagrant - http://downloads.vagrantup.com/ 

## Clone repo

> \# cd ~
> \# git clone http://bitbucket.org/cfarquhar/openstack-vagrant
> \# cd openstack-vagrant

## Initialize persistent data
 * This pre-downloads stuff that we don't want to download multiple times (cookbooks, chef-server package, glance images, etc)
 * Takes about 6 minutes to complete

> \# ./init-persistent-data.sh

## Run 'vagrant up'
 * This will give you a fully functional environment with a vyatta router providing vlans, a controller, and two compute nodes using a combined host/provider NIC.  Networks/subnets, glance images, and keys are already set up.  Just run 'nova boot ...'.
 * Takes about 42 minutes to complete on my laptop.  Will run a little longer the first time as we have to download the vagrant base images from cloud files.
 * While this is running, scan through the following files to see what's going on:
  * ./Vagrantfile
  * ./scripts/*

> \# vagrant up

# Tips
## Vagrant
 * Use `vagrant destroy --force; vagrant up` to rebuild everything
 * Use `vagrant up <node>` and 'vagrant destroy --force <node>` to manage a single node
 * Use `vagrant ssh {router,controller01,compute01,compute02}` to ssh to a node
 * Uncomment `vb.gui = true` in ./Vagrantfile if you need a GUI console on the node for any reason

## Misc
 * Horizon can be accessed at https://localhost:8443/

## Credentials
 * router - vyatta/vyatta
 * controller/compute nodes - vagrant/vagrant
 * OpenStack user - admin/password

## Limitations
 * This is alpha
 * Nothing has been modularized yet.  Not sure how to do this and keep the deployment scripts similar to our deployment process.
 * Need to think about templating and adding support for
  * HA controllers
  * nova-network
  * cinder
  * swift
  * separate host/provider NICs
  * bonding
